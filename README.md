# Android Quantum Random Number Picker

This is an example random number picker for Android, with two functions:
-It opens into an activity that picks a random float.
-In the activity bar menu, it can switch to an activity that picks a natural number between 1 and whatever number is input.
(The second activity automatically decrements the maximum number, which can make it convenient for permutation, such as for shuffling cards.)

Pressing the left button adds random numbers to cache. Pressing the right button draws numbers from cache.

Why bother with this basic example, that lacks design, and has no obvious special functions? Because the example draws quantum random numbers from a Web API from the Australian National University:
https://qrng.anu.edu.au/
The underlying data source is "high quality," "true" random numbers. This might be important to some people for some applications. (The example algorithm for picking an integer, however, is not high quality.)

The project is ready to compile directly to an Android phone.
package universesplitter.frogseyeview.net.universesplitter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.anuqrng.generation.QuantumRandom;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;
        final SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preferences_file_key), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        //editor.putString(getString(R.string.preferences_cache_key), "");

        final QuantumRandom qrngClient = new QuantumRandom();

        final TextView tvCache = (TextView) findViewById(R.id.tvCache);
        String cache = sharedPref.getString(getString(R.string.preferences_cache_key), "");
        int byteCount = Base64.decode(cache, Base64.NO_WRAP).length;
        tvCache.setText(Integer.toString(byteCount) + " bytes in cache");

        final TextView tvNumber = (TextView) findViewById(R.id.tvNumber);

        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cache = sharedPref.getString(getString(R.string.preferences_cache_key), "");
                byte[] data = new byte[100];
                qrngClient.nextBytes(data);
                byte[] oldData = Base64.decode(cache,Base64.NO_WRAP);
                byte[] fullData = new byte[100 + oldData.length];
                for (int i = 0; i < oldData.length; i++) {
                    fullData[i] = oldData[i];
                }
                for (int i = 0; i < 100; i++) {
                    fullData[oldData.length + i] = data[i];
                }
                cache = Base64.encodeToString(fullData, Base64.NO_WRAP);
                editor.putString(getString(R.string.preferences_cache_key), cache);
                editor.commit();
                int byteCount = cache.length();
                tvCache.setText(Integer.toString(fullData.length) + " bytes in cache");
                Snackbar.make(view, "Added 100 bytes to cache", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cacheStr = sharedPref.getString(getString(R.string.preferences_cache_key), "");
                byte[] cache = Base64.decode(cacheStr, Base64.NO_WRAP);
                if (cache.length < 4) {
                    tvNumber.setText("Not enough bytes in cache.");
                }
                else {
                    byte[] bytes = new byte[4];
                    for (int i = 0; i < 4; i++) {
                        bytes[i] = cache[i];
                    }
                    byte[] cacheAfter = new byte[cache.length - 4];
                    for (int i = 0; i < cacheAfter.length; i++) {
                        cacheAfter[i] = cache[i + 4];
                    }
                    double value = 0.0;
                    for (int i = 0; i < 4; i++) {
                        value += ((double) bytes[i] + 128) / Math.pow(256, i + 1);
                    }
                    Calendar now = Calendar.getInstance();
                    SimpleDateFormat format1 = new SimpleDateFormat("H:mm:ss");
                    String timeStr = format1.format(now.getTime());
                    tvNumber.setText(Double.toString(value) + " at " + timeStr);
                    cacheStr = Base64.encodeToString(cacheAfter, Base64.NO_WRAP);
                    editor.putString(getString(R.string.preferences_cache_key), cacheStr);
                    editor.commit();
                    tvCache.setText(Integer.toString(cacheAfter.length) + " bytes in cache");
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        Context context = this;
        final SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preferences_file_key), Context.MODE_PRIVATE);
        final TextView tvCache = (TextView) findViewById(R.id.tvCache);
        String cache = sharedPref.getString(getString(R.string.preferences_cache_key), "");
        int byteCount = Base64.decode(cache, Base64.NO_WRAP).length;
        tvCache.setText(Integer.toString(byteCount) + " bytes in cache");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem shuffleItem = menu.findItem(R.id.action_card_shuffle);
        shuffleItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(context, ShuffleActivity.class);
                startActivity(intent);
                return true;
            }
        });

        return true;
    }
}

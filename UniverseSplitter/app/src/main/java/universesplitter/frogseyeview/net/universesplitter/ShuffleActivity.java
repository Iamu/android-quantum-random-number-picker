package universesplitter.frogseyeview.net.universesplitter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.anuqrng.generation.QuantumRandom;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ShuffleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shuffle);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Context context = this;
        final SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preferences_file_key), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        //editor.putString(getString(R.string.preferences_cache_key), "");

        final QuantumRandom qrngClient = new QuantumRandom();

        final TextView tvCache = (TextView) findViewById(R.id.tvCache);
        String cache = sharedPref.getString(getString(R.string.preferences_cache_key), "");
        int byteCount = Base64.decode(cache, Base64.NO_WRAP).length;
        tvCache.setText(Integer.toString(byteCount) + " bytes in cache");

        final TextView tvNumber = (TextView) findViewById(R.id.tvNumber);

        final EditText etOptionCount = (EditText) findViewById(R.id.etOptionCount);
        etOptionCount.setText("78");

        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cache = sharedPref.getString(getString(R.string.preferences_cache_key), "");
                byte[] data = new byte[100];
                qrngClient.nextBytes(data);
                byte[] oldData = Base64.decode(cache,Base64.NO_WRAP);
                byte[] fullData = new byte[100 + oldData.length];
                for (int i = 0; i < oldData.length; i++) {
                    fullData[i] = oldData[i];
                }
                for (int i = 0; i < 100; i++) {
                    fullData[oldData.length + i] = data[i];
                }
                cache = Base64.encodeToString(fullData, Base64.NO_WRAP);
                editor.putString(getString(R.string.preferences_cache_key), cache);
                editor.commit();
                int byteCount = cache.length();
                tvCache.setText(Integer.toString(fullData.length) + " bytes in cache");
                Snackbar.make(view, "Added 100 bytes to cache", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cacheStr = sharedPref.getString(getString(R.string.preferences_cache_key), "");
                byte[] cache = Base64.decode(cacheStr, Base64.NO_WRAP);

                int optionCount = 2;
                String optionCountStr = etOptionCount.getText().toString();
                try {
                    optionCount = Integer.parseInt(optionCountStr);
                }
                catch (Error e) {
                    tvNumber.setText("Enter an integer.");
                    return;
                }

                boolean isPow2 = true;
                int powOf2 = 0;
                int testPowOf2 = optionCount;
                while (isPow2 && testPowOf2 > 1) {
                    if (((int)(testPowOf2/2))*2 == testPowOf2) {
                        testPowOf2 = testPowOf2 / 2;
                        powOf2++;
                    }
                    else {
                        isPow2 = false;
                    }
                }
                String boolString = sharedPref.getString(getString(R.string.preferences_bool_key), "");
                int selected;
                byte[] cacheAfter;

                if ((!isPow2 && cache.length < 4) || (isPow2 && (cache.length * 8 + boolString.length()) < powOf2)) {
                    tvNumber.setText("Not enough bytes in cache.");
                }
                else {
                    if (isPow2) {
                        boolean[] bools = new boolean[powOf2];
                        int boolIndex = 0;
                        int cacheIndex = 0;
                        testPowOf2 = powOf2;
                        while (testPowOf2 >= 8) {
                            int currentByte = cache[cacheIndex] + 128;
                            cacheIndex++;
                            for (int j = 0; j < 8; j++) {
                                int testPow = (int) Math.pow(2, 7 - j);
                                if (currentByte >= testPow) {
                                    currentByte -= testPow;
                                    bools[boolIndex] = true;
                                } else {
                                    bools[boolIndex] = false;
                                }
                                boolIndex++;
                            }
                            testPowOf2 -= 8;
                        }
                        if (testPowOf2 > boolString.length()) {
                            int currentByte = cache[cacheIndex] + 128;
                            cacheIndex++;
                            for (int j = 0; j < 8; j++) {
                                int testPow = (int) Math.pow(2, 7 - j);
                                if (currentByte >= testPow) {
                                    currentByte -= testPow;
                                    boolString = boolString + "1";
                                } else {
                                    boolString = boolString + "0";
                                }
                            }
                        }
                        for (int j = 0; j < testPowOf2; j++) {
                            char currentChar = boolString.charAt(0);
                            boolString = boolString.substring(1);
                            if (currentChar == '1') {
                                bools[boolIndex] = true;
                            } else {
                                bools[boolIndex] = false;
                            }
                            boolIndex++;
                        }

                        selected = 1;
                        for (int j = 0; j < bools.length; j++) {
                            if (bools[j]) {
                                selected += Math.pow(2, j);
                            }
                        }

                        cacheAfter = new byte[cache.length - cacheIndex];
                        for (int i = 0; i < cacheAfter.length; i++) {
                            cacheAfter[i] = cache[i + cacheIndex];
                        }

                        editor.putString(getString(R.string.preferences_bool_key), boolString);
                    } else {
                        byte[] bytes = new byte[4];
                        for (int i = 0; i < 4; i++) {
                            bytes[i] = cache[i];
                        }
                        cacheAfter = new byte[cache.length - 4];
                        for (int i = 0; i < cacheAfter.length; i++) {
                            cacheAfter[i] = cache[i + 4];
                        }
                        double value = 0.0;
                        for (int i = 0; i < 4; i++) {
                            value += ((double) bytes[i] + 128) / Math.pow(256, i + 1);
                        }
                        selected = (int) (value * optionCount) + 1;
                    }

                    cacheStr = Base64.encodeToString(cacheAfter, Base64.NO_WRAP);
                    editor.putString(getString(R.string.preferences_cache_key), cacheStr);
                    editor.commit();
                    tvCache.setText(Integer.toString(cacheAfter.length) + " bytes in cache");


                    Calendar now = Calendar.getInstance();
                    SimpleDateFormat format1 = new SimpleDateFormat("H:mm:ss");
                    String timeStr = format1.format(now.getTime());
                    tvNumber.setText(Integer.toString(selected) + " / " + Integer.toString(optionCount) + " at " + timeStr);
                    if (optionCount > 2) {
                        etOptionCount.setText(Integer.toString(optionCount - 1));
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shuffle, menu);

        MenuItem shuffleItem = menu.findItem(R.id.action_double);
        shuffleItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                finish();
                return true;
            }
        });

        return true;
    }
}

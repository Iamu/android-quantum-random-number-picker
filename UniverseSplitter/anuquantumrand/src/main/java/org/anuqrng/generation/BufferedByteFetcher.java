package org.anuqrng.generation;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 *
 * @author Brian
 */
class BufferedByteFetcher implements ByteFetcher {

    private final ByteBuffer buffer;

    public BufferedByteFetcher(int size) throws IOException {
        buffer = ByteBuffer.allocate(size);
        fillBuffer();
    }

    public BufferedByteFetcher() throws IOException {
        this(Generation.BUFFER_SIZE);
    }

    @Override
    public byte nextByte() throws IOException {
        synchronized (buffer) { // ByteBuffer isn't thread-safe
            if (buffer.hasRemaining()) {
                return buffer.get();
            } else {
                fillBuffer();
                return buffer.get();
            }
        }
    }

    @Override
    public byte[] nextBytes(int count) throws IOException {
        synchronized (buffer) { // ByteBuffer isn't thread-safe
            byte[] bytes = new byte[count];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = nextByte();
            }
            return bytes;
        }
    }

    private void fillBuffer() throws IOException {
        GenerationUtil.fillJson(buffer.array());
        buffer.rewind();
    }
}

package org.anuqrng.generation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author Brian
 */
class GenerationUtil {

    private static final String PROTOCOL = "http";
    private static final String QRNG_IP = "150.203.48.55";
    private static final String JSON_1 = "/API/jsonI.php?type=hex16&time=";
    private static final String JSON_2 = "&size=1024&length=";
    private static final String CHARSET_STRING = "charset=";
    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final Gson GSON;

    static {
        GsonBuilder builder = new GsonBuilder();
        GSON = builder.create();
    }

    public static void fillJson(byte[] bytes) throws IOException {
        if (bytes.length == 0) {
            return;
        }
        URL url = getJsonRequestUrl(bytes.length);
        HttpURLConnection httpConnection = connect(url);
        InputStream in = getInputStream(httpConnection);
        String charset = getCharset(httpConnection);
        Reader reader = new InputStreamReader(in, charset);
        JsonResponse response = GSON.fromJson(reader, JsonResponse.class);
        if (response.isSuccess()) {
            fill(response.getData(), bytes);
        } else {
            throw new IOException("Failed to retrieve bytes from JSON (success field was false)");
        }
    }

    private static URL getJsonRequestUrl(int byteCount) throws MalformedURLException {
        int stringCount = getStringCount(byteCount);
        return new URL(PROTOCOL, QRNG_IP, JSON_1 + System.currentTimeMillis() + JSON_2 + stringCount);
    }

    private static int getStringCount(int byteCount) {
        int initialByteCount = byteCount / 1024;
        return byteCount % 1024 == 0 ? initialByteCount : initialByteCount + 1;
    }

    private static HttpURLConnection connect(URL url) throws IOException {
        URLConnection connection = url.openConnection();
        HttpURLConnection httpConnection = (HttpURLConnection) connection;
        httpConnection.connect();
        return httpConnection;
    }

    private static InputStream getInputStream(HttpURLConnection httpConnection) throws IOException {
        Object content = httpConnection.getContent();
        InputStream in;
        if (content instanceof InputStream) {
            in = (InputStream) content;
        } else {
            throw new IOException("Invalid content; stream expected");
        }
        return in;
    }

    private static String getCharset(HttpURLConnection httpConnection) {
        String contentType = httpConnection.getContentType();
        int charsetIndex = contentType.indexOf(CHARSET_STRING);
        String charset = DEFAULT_CHARSET;
        if (charsetIndex > 0) {
            charsetIndex += CHARSET_STRING.length();
            charset = contentType.substring(charsetIndex);
        }
        return charset;
    }

    private static void fill(String[] data, byte[] bytes) {
        int fillCount = 0;
        for (String datum : data) {
            if (fillCount >= bytes.length) {
                break;
            }
            int length = bytes.length - fillCount;
            int datumByteCount = datum.length() / 2;
            length = length < datumByteCount ? length : datumByteCount;
            fill(datum, fillCount, length, bytes);
            fillCount += length;
        }
    }

    private static void fill(String datum, int offset, int length, byte[] bytes) {
        char[] charArray = datum.toCharArray();
        for (int i = 0;
                i < charArray.length
                && offset + i / 2 < offset + length;
                i += 2) {
            char c1 = charArray[i];
            char c2 = charArray[i + 1];
            int b = Character.digit(c1, 16) * 16 + Character.digit(c2, 16);
            bytes[offset + i / 2] = (byte) b;
        }
    }

    private static class JsonResponse {

        private String type;
        private int length;
        private int size;
        private String[] data;
        private boolean success;

        public String[] getData() {
            return data;
        }

        public int getLength() {
            return length;
        }

        public int getSize() {
            return size;
        }

        public String getType() {
            return type;
        }

        public boolean isSuccess() {
            return success;
        }
    }

    private GenerationUtil() {
    }
}
